## Donation App

This is Simple Microservices project using Laravel and Spring Boot, for messagin, im use Redis as Message Broker

<img src='showcase/structure.png'>


## Authentication
I save token that generated from users-service on redis.
<img src='showcase/redis.png'>

if client hit campaign-service and donation service, the middleware and interceptor will check token saved on header, if same with the token on redis, client will be in, but if different, client cannot in.


this apps is not totally perfect, so merge request is very welcome.